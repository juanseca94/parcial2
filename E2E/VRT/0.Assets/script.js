/* VARIABLES */
var urlServer = "http://127.0.0.1:8080";
//var urlServer = "http://172.21.58.225:8080";

/* UTILIDADES */
function getParameterURL(parameter) {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(parameter);
}

/* MENSAJES DE ALERTA TIPO MODAL */
function CreateAlert() {
	var buttonAlert = document.createElement("button");
	buttonAlert.setAttribute("id", "Alert");
	buttonAlert.setAttribute("name", "Alert");
	buttonAlert.setAttribute("class", "btn btn-default hidden");
	buttonAlert.setAttribute("data-toggle", "modal");
	buttonAlert.setAttribute("data-target", "#alertModal");
	document.body.appendChild(buttonAlert);

	var divAlert = document.createElement("div");
	divAlert.setAttribute("id", "alertModal");
	divAlert.setAttribute("class", "modal fade");
	divAlert.setAttribute("tabindex", "-1");
	divAlert.setAttribute("role", "dialog");
	divAlert.setAttribute("aria-labelledby", "headerModal");
	document.body.appendChild(divAlert);

	document.getElementById("alertModal").innerHTML = ""+
		"<div class='modal-dialog' role='document'>" +
	  	"<div class='modal-content'>" +
	    		"<div class='modal-header modalHeader'>" +
	      		"<button type='button' class='close modalBtnClose' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
	      		"<h4 class='modal-title' name='titleAlert' id='titleAlert'></h4>" +
	    		"</div>" +
	    		"<div class='modal-body modalBody'>" +
	    			"<p id='txtAlert' name='txtAlert'></p>" +
					"<textarea class='form-control' id='inputAlert' name='inputAlert' rows='3'></textarea>" +
					"<div class='modal-body modalBody' id='personalizeAlert' name='personalizeAlert'>" +
		    		"</div>" +
	  		"</div>" +
			    "<div class='modal-footer'>" +
			        "<button id='btnDefaultAlert' name='btnDefaultAlert' type='button' class='btn modalBtnDefault' data-dismiss='modal'></button>" +
			        "<button id='btnPrimaryAlert' name='btnPrimaryAlert' type='button' class='btn modalBtnPrimary' data-dismiss='modal'></button>" +
			    "</div>" +
	  	"</div>" +
		"</div>";
}

function Alert(titulo,texto,tipo,botones){
	CreateAlert();
	
	$('#btnPrimaryAlert').off('click');
	$('#btnDefaultAlert').off('click');
	//Los nombres de los botones separados por comas
	var botones = botones.split(",");
	//Alerta normal, solo muestra el texto y un boton
	if(tipo=="alert"){
		$('#titleAlert').html(titulo);
	    $('#txtAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').hide();
	    $('#inputAlert').hide();
	    $('#personalizeAlert').hide();      
	}
	//Alerta de tipo confirm, texto con pregunta y dos botones
	if(tipo=="confirm"){
	    $('#titleAlert').html(titulo);
	    $('#txtAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').show();
	    $('#btnDefaultAlert').html(botones[1]);
	    $('#inputAlert').hide();
	    $('#personalizeAlert').hide();     
	}
	//Alerta de tipo input, dos botones y una caja de texto (inputAlert)
	if(tipo=="input"){
	    $('#titleAlert').html(titulo);
	    $('#txtAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').show();
	    $('#btnDefaultAlert').html(botones[1]);
	    $('#inputAlert').show();
	    $('#personalizeAlert').hide();       
	}
	//Alerta personalizable con contenido propio enviando el html
	if(tipo=="personalize"){
	    $('#titleAlert').html(titulo);
	    $('#txtAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').show();
	    $('#btnDefaultAlert').html(botones[1]);
	    $('#inputAlert').hide();
	    $('#personalizeAlert').show();        
	}
	$("#alertModal").modal('show');
}

var btnPrimaryAlert = document.getElementById('btnPrimaryAlert');
if (btnPrimaryAlert != null) {
	btnPrimaryAlert.addEventListener('click',function(){
		document.body.removeChild(buttonAlert);
		document.body.removeChild(divAlert);
	})
}

/* MENSAJES DE ALERTA TIPO MODAL */
function CreateAlert() {
	var buttonAlert = document.createElement("button");
	buttonAlert.setAttribute("id", "Alert");
	buttonAlert.setAttribute("name", "Alert");
	buttonAlert.setAttribute("class", "btn btn-default hidden");
	buttonAlert.setAttribute("data-toggle", "modal");
	buttonAlert.setAttribute("data-target", "#alertModal");
	document.body.appendChild(buttonAlert);

	var divAlert = document.createElement("div");
	divAlert.setAttribute("id", "alertModal");
	divAlert.setAttribute("class", "modal fade");
	divAlert.setAttribute("tabindex", "-1");
	divAlert.setAttribute("role", "dialog");
	divAlert.setAttribute("aria-labelledby", "headerModal");
	document.body.appendChild(divAlert);

	document.getElementById("alertModal").innerHTML = ""+
		"<div id='mdalert' class='modal-dialog' role='document'>" +
		  	"<div class='modal-content'>" +
		    		"<div class='modal-header modalHeader'>" +
		      		"<button type='button' class='close modalBtnClose' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
		      		"<h4 class='modal-title' name='titleAlert' id='titleAlert'></h4>" +
		    		"</div>" +
		    		"<div class='modal-body modalBody'>" +
		    			"<p id='txtAlert' name='txtAlert'></p>" +
						"<textarea class='form-control' id='inputAlert' name='inputAlert' rows='3'></textarea>" +
						"<div class='modal-body modalBody' id='personalizeAlert' name='personalizeAlert'>" +
			    		"</div>" +
			    	"</div>" +
				    "<div class='modal-footer'>" +
				        "<button id='btnDefaultAlert' name='btnDefaultAlert' type='button' class='btn modalBtnDefault' data-dismiss='modal'></button>" +
				        "<button id='btnPrimaryAlert' name='btnPrimaryAlert' type='button' class='btn modalBtnPrimary' data-dismiss='modal'></button>" +
				    "</div>" +
		  	"</div>" +
		"</div>";
}

function Alert(titulo,texto,tipo,botones,ancho){
	CreateAlert();
	
	$('#btnPrimaryAlert').off('click');
	$('#btnDefaultAlert').off('click');
	//Los nombres de los botones separados por comas
	var botones = botones.split(",");
	//Alerta normal, solo muestra el texto y un boton
	if(tipo=="alert"){
		$('#titleAlert').html(titulo);
	    $('#txtAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').hide();
	    $('#inputAlert').hide();
	    $('#personalizeAlert').hide();      
	}
	//Alerta de tipo confirm, texto con pregunta y dos botones
	if(tipo=="confirm"){
	    $('#titleAlert').html(titulo);
	    $('#txtAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').show();
	    $('#btnDefaultAlert').html(botones[1]);
	    $('#inputAlert').hide();
	    $('#personalizeAlert').hide();     
	}
	//Alerta de tipo input, dos botones y una caja de texto (inputAlert)
	if(tipo=="input"){
	    $('#titleAlert').html(titulo);
	    $('#txtAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').show();
	    $('#btnDefaultAlert').html(botones[1]);
	    $('#inputAlert').show();
	    $('#personalizeAlert').hide();       
	}
	//Alerta personalizable con contenido propio enviando el html
	if(tipo=="personalize"){
	    $('#titleAlert').html(titulo);
	    $('#personalizeAlert').html(texto);
	    $('#btnPrimaryAlert').html(botones[0]);
	    $('#btnDefaultAlert').hide();
	    if (botones.length > 1) {
		    $('#btnDefaultAlert').show();
		    $('#btnDefaultAlert').html(botones[1]);
	    }
	    $('#txtAlert').hide();
	    $('#inputAlert').hide();
	    $('#personalizeAlert').show();    
	    $('.modal-footer').css("border","none");
	    $('#mdalert').width(ancho);   
	}
	$("#alertModal").modal('show');
}

var btnPrimaryAlert = document.getElementById('btnPrimaryAlert');
if (btnPrimaryAlert != null) {
	btnPrimaryAlert.addEventListener('click',function(){
		document.body.removeChild(buttonAlert);
		document.body.removeChild(divAlert);
	})
}



// Footer buttons
var irApp = document.getElementById('ir-app');
if (irApp != null) {
	irApp.addEventListener('click',function(){
		window.location.href = "../Apps/index.html";
	})
}
var irDispo = document.getElementById('ir-dispo');
if (irDispo != null) {
	irDispo.addEventListener('click',function(){
		window.location.href = "../Dispositivos/index.html";
	})
}
var irFeature = document.getElementById('ir-feature');
if (irFeature != null) {
	irFeature.addEventListener('click',function(){
		window.location.href = "../Features/index.html";
	})
}
var irSuite = document.getElementById('ir-suite');
if (irSuite != null) {
	irSuite.addEventListener('click',function(){
		window.location.href = "../Suites/index.html";
	})
}
var irNodo = document.getElementById('ir-nodo');
if (irNodo != null) {
	irNodo.addEventListener('click',function(){
		window.location.href = "../Nodos/index.html";
	})
}
var irEjec = document.getElementById('ir-ejec');
if (irEjec != null) {
	irEjec.addEventListener('click',function(){
		window.location.href = "../Ejecuciones/index.html";
	})
}
var irVrt = document.getElementById('ir-vrt');
if (irVrt != null) {
	irVrt.addEventListener('click',function(){
		window.location.href = "../VRT/index.html";
	})
}


String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};