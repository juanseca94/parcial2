package app.webapi.controller;

import java.io.File;
import java.io.IOException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.webapi.model.PairImagesVRT;
import app.webapi.util.FileManager;
import app.webapi.util.ImagesEncode;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class VrtREST2 {
	
	@GetMapping("/vrt2/getPairOfImagesForVRT/{currentStepNum}/{mutantNum}")
	public PairImagesVRT getPairOfImagesForVRT2(@PathVariable String currentStepNum, @PathVariable String mutantNum) {
		String reporte1 = "C:\\Users\\MagisAdmin\\Desktop\\Parcial Final\\output";
		String reporte2 = "C:\\Users\\MagisAdmin\\Desktop\\Parcial Final\\output"+mutantNum;
		
		String notImage = "t30.png";
		
		PairImagesVRT pairImagesVRT = new PairImagesVRT();
		String path1 = reporte1+"/"+currentStepNum+".png";
		pairImagesVRT.setData1(ImagesEncode.getBytesFromFile(path1));
		String path2 = reporte2+"/"+currentStepNum+".png";
		if (!(new File(path2).exists())) {
			path2 = reporte1+"/"+notImage;
		} 
		pairImagesVRT.setData2(ImagesEncode.getBytesFromFile(path2));
		return pairImagesVRT;
	}
	
	@GetMapping("/vrt3/getPairOfImagesForVRT/{currentStepNum}/{mutantNum}")
	public PairImagesVRT getPairOfImagesForVRT3(@PathVariable String currentStepNum, @PathVariable String mutantNum) {
		PairImagesVRT pairImagesVRT = new PairImagesVRT();
		
		String reportsFolderPath = "C:\\Users\\JUANSECA\\Documents\\eclipse-workspace\\AutomatorQ\\reports";
		File reportsFolder = new File(reportsFolderPath);
		File[] reportsFolderList = reportsFolder.listFiles();
		for (File reportFolder : reportsFolderList) {
			if (reportFolder.getName().contains("Parcial2-Móvil_Mutante"+mutantNum+"_")) {
				String reporte1 = "C:\\Users\\JUANSECA\\Documents\\eclipse-workspace\\AutomatorQ\\reports\\Parcial2-Móvil_2019-05-27_11.02.16";
				String reporte2 = "C:\\Users\\JUANSECA\\Documents\\eclipse-workspace\\AutomatorQ\\reports\\"+reportFolder.getName();
				
				String notImage = "notImage.png";
				
				String path1 = reporte1+"/screenshots/Creación_de_tanqueada,_revisión_de_historial_y_estadísticas/"+currentStepNum+".png";
				pairImagesVRT.setData1(ImagesEncode.getBytesFromFile(path1));
				String path2 = reporte2+"/screenshots/Creación_de_tanqueada,_revisión_de_historial_y_estadísticas/"+currentStepNum+".png";
				System.out.println("Reporte1: "+path1);
				System.out.println("Reporte2: "+path2);
				if (!(new File(path2).exists())) {
					System.out.println("No existe!!");
					path2 = reporte1+"/screenshots/"+notImage;
				} 
				pairImagesVRT.setData2(ImagesEncode.getBytesFromFile(path2));
				
				break;
			}
		}
		
		return pairImagesVRT;
	}
	
//	public static void main(String[] args) throws IOException {
//		String folderPath = "C:\\Users\\JUANSECA\\Documents\\eclipse-workspace\\AutomatorQ\\reports";
//		File folder = new File(folderPath);
//		File[] subFolders = folder.listFiles();
//		for (File subFolder : subFolders) {
//			File subFolder2 = new File(subFolder.getPath()+"/screenshots");
//			if (subFolder2.isDirectory()) {
//				File[] subsubFolders = subFolder2.listFiles();
//				for (File subsubFolder : subsubFolders) {
//					String originalFolderPath = subsubFolder.getPath();
//					String newFolderPath = originalFolderPath.replaceAll("estadísticas_2", "estadísticas");
//					
//					System.out.println("Folder: "+originalFolderPath);
//					if (originalFolderPath.contains("estadísticas_2")) {
//						System.out.println("Entre...");
//						System.out.println("originalFolderPath: "+originalFolderPath);
//						System.out.println("newFolderPath: "+newFolderPath);
//						
//						FileManager.copyContentFromOriginFolderInToNewFolder(originalFolderPath, newFolderPath);
//						FileManager.deleteContentFromOriginFolder(originalFolderPath);
//					}
//					break;
//				}
//			}
//		}
//	}
	
}
