$(function () {
    var apiRestGetPairImgs = "api/vrt2/getPairOfImagesForVRT/";
    var currentDate = "";
    var reports = 0;
    var steps;
    var stepsDiff;
    var globalMismatch;

    var mutante;
    var m = 0;
    var mLimit = 750;

    function getDateInFormat_ddmmyyyyhhmmss() {
        now = new Date();
        year = "" + now.getFullYear();
        month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
        day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
        hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
        minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
        second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
        return day + "-" + month + "-" + year + " " + hour + ":" + minute + ":" + second;
    }

    function convertImgBase64ToFile(dataurl) {
        var arr = dataurl.split(',');
        var mime = arr[0].match(/:(.*?);/)[1];
        var bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], 'a.png', { type: mime });
    }

    function onComplete(data) {
        $("#report-" + reports).find("#mutante").html(mutante);
        $("#report-" + reports).find("#fecha-reporte").html(currentDate);

        var diffImage = new Image();
        diffImage.src = data.getImageDataUrl();
        diffImage.className = "img-fluid img-thumbnail img-diff";

        var link = document.createElement('a');
        link.href = diffImage.src;
        link.download = "report" + reports + "_diffImg" + stepsDiff + "_" + currentDate + ".png";
        link.appendChild(diffImage);

        $("#report-" + reports).find("#step-" + stepsDiff).find("#img-diff").html(link);
        $("#report-" + reports).find("#step-" + stepsDiff).find("#label-info").hide();

        if (data.misMatchPercentage == 0) {
            $("#report-" + reports).find("#step-" + stepsDiff).find("#diff-results-equals").show();
            $("#report-" + reports).find("#step-" + stepsDiff).find("#diff-results").hide();
        } else {
            globalMismatch = parseFloat(globalMismatch + parseFloat(data.misMatchPercentage));
            $("#report-" + reports).find("#step-" + stepsDiff).find("#mismatch").text(data.misMatchPercentage);
            if (!data.isSameDimensions) {
                $("#report-" + reports).find("#step-" + stepsDiff).find("#diff-results-different-dimensions").show();
            } else {
                $("#report-" + reports).find("#step-" + stepsDiff).find("#diff-results-different-dimensions").hide();
            }
            $("#report-" + reports).find("#step-" + stepsDiff).find("#diff-results").show();
            $("#report-" + reports).find("#step-" + stepsDiff).find("#diff-results-equals").hide();
        }
        if (steps == stepsDiff) {
            var maxError = 10;
            if ((globalMismatch / steps) < maxError) {
                $("#report-" + reports).find("#estado-reporte").html("CORRECTO");
                $("#report-" + reports).removeClass("panel-info");
                $("#report-" + reports).addClass("panel-success");
                console.log("Mutante "+mutante+" CORRECTO");
            } else {
                $("#report-" + reports).find("#estado-reporte").html("FALLIDO");
                $("#report-" + reports).removeClass("panel-info");
                $("#report-" + reports).addClass("panel-danger");
                console.log("Mutante "+mutante+" FALLIDO");
            }
            $("#report-" + reports).find("#errorM").html((globalMismatch / steps).toFixed(1) + " %");

            // Guarda html
            $("#new-report").hide();
            var blob = new Blob([$("html").html()], {
                type: "text/html;charset=utf-8"
            });
            saveAs(blob, "vrtMutante" + mutante + ".html");

            // Elimina reporte actual
            var elem = document.querySelector('#report-'+reports);
            elem.parentNode.removeChild(elem);

            // Siguiente reporte
            m = m + 1;
            if (m < mLimit) {
                newReport();
            }
        } else {
            stepsDiff = stepsDiff + 1;
        }
    }

    function saveAs(content, filename) {
        var element = document.createElement('a');
        element.setAttribute('href', window.URL.createObjectURL(content));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

    $("#new-report").click(function () {
        newReport();
    });

    function newReport() {
        var divReports = document.getElementById("reports");
        var originReport = document.getElementById('report-0');
        originReport.style.display = "none";

        var mutantInit = 1000;

        mutante = mutantInit + (m + 1);

        var stepsNum = 9;
        steps = 0;

        stepsDiff = 1;
        globalMismatch = 0;

        reports = reports + 1;

        var cloneReport = originReport.cloneNode(true);
        cloneReport.id = "report-" + reports;
        cloneReport.style.display = "block";

        divReports.insertBefore(cloneReport, originReport);

        var currentReport = document.getElementById('report-'+reports);

        currentDate = getDateInFormat_ddmmyyyyhhmmss();

        $("#new-report").hide();
        $("#wait-report").show();

        if (stepsNum > 0) {
            for (let i = 0; i < stepsNum; i++) {
                $.ajax({
                    url: urlServer + "/" + apiRestGetPairImgs + "/" + (i + 1) + "/" + mutante,
                    type: 'get',
                    async: false
                }).then(function (data) {
                    var divSteps = currentReport.querySelector("#steps");
                    var currentStep = currentReport.querySelector("#step-" + steps);
                    var cloneStep = currentReport.querySelector('#step-0').cloneNode(true);

                    steps = steps + 1;
                    cloneStep.id = "step-" + steps;
                    cloneStep.style.display = "block";

                    divSteps.insertBefore(cloneStep, currentStep);
                    if ((i + 1) == 1) {
                        currentStep.style.display = "none";
                    }

                    $("#report-" + reports).find("#step-num").html('Step #' + (i + 1));

                    var img1Base64 = "data:image/png;base64, " + data.data1;
                    var nameDownloadImg = "report" + reports + "_img1_" + currentDate;
                    $("#report-" + reports).find("#img-1").html('<a href="' + img1Base64 + '" download="' + nameDownloadImg + '.png"><img src="' + img1Base64 + '" class="img-fluid img-thumbnail img"/></a>');

                    var img2Base64 = "data:image/png;base64, " + data.data2;
                    var nameDownloadImg = "report" + reports + "_img2_" + currentDate;
                    $("#report-" + reports).find("#img-2").html('<a href="' + img2Base64 + '" download="' + nameDownloadImg + '.png"><img src="' + img2Base64 + '" class="img-fluid img-thumbnail img"/></a>');

                    var file1 = convertImgBase64ToFile(img1Base64);
                    var file2 = convertImgBase64ToFile(img2Base64);

                    resembleControl = resemble(file1)
                        .compareTo(file2)
                        .onComplete(onComplete);

                    $("#wait-report").hide();
                    $("#new-report").show();
                });
            }
        } else {
            $("#wait-report").hide();
            $("#new-report").show();

            Alert("¡Acción invalida!", "No fue posible encontrar el escenario!!", "alert", "Aceptar");
        }
    }
});
