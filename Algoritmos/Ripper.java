package com.test.runner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class GenerateMutants3 {

	public static void main(String[] args) {
		String mutantsFolderPath = "C:\\Users\\JUANSECA\\Desktop\\Parcial Final\\Apks";
		int mutantIni = 1601;
		int mutants = 150;
		
		String commandRunDirectory = "C:\\Users\\JUANSECA\\Desktop\\Parcial Final\\Ripper";
		String commandRun = "Java -jar RIP.jar rip_config.json";
		
		File mutantsFolder = new File(mutantsFolderPath);
		File[] mutantsFiles = mutantsFolder.listFiles();
		if (mutantsFiles != null) {
			for (int i=mutantIni; i<(mutantIni+mutants); i++) {
				System.out.println("Corriendo mutante "+(i+1));
				
				String apkPath = "../Apks/com.evancharlton.mileage-mutant"+(i+1)+"/com.evancharlton.mileage_3110-aligned-debugSigned.apk";
				String outputFolder = "../output"+(i+1)+"/";
				
				try {
					// Modifica rip_config
					File configFile = new File(commandRunDirectory+"\\rip_config.json");
					List<String> readFeature = new ArrayList<String>();
					
					FileReader fileR = null;
					BufferedReader input = null;
					fileR = new FileReader(configFile);  
					if (fileR != null) {
						input = new BufferedReader(fileR);
						
						String line;
				        while ((line = input.readLine()) != null) {
				        	if (line.contains("apkPath")) {
				        		readFeature.add("\"apkPath\":\""+apkPath+"\",");
				        	} else if (line.contains("outputFolder")) {
				        		readFeature.add("\"outputFolder\":\""+outputFolder+"\",");
				        	} else {
				        		readFeature.add(line);
				        	}
					    }
				        if(fileR != null) fileR.close();
				        input.close();
					}
					
					// Elimina config viejo
					configFile.delete();
					
					// Crea config nuevo
					FileWriter fichero = null;
			        PrintWriter pw = null;
			        fichero = new FileWriter(configFile);
			        pw = new PrintWriter(fichero);
			        for (String lineC : readFeature) {
			        	pw.println(lineC);
					}
			        fichero.close();
			        
			        // Desinstala app vieja
			        String uninstallApp = "adb shell pm uninstall com.evancharlton.mileage";
			        String sdkFolder = "C:\\Users\\JUANSECA\\AppData\\Local\\Android\\Sdk\\emulator";
			        ProcessBuilder builderApp = new ProcessBuilder();
					builderApp.directory(new File(sdkFolder));
					builderApp.command("cmd.exe", "/c", uninstallApp);
				    
					String lineApp; 
				    Process stateRunApp = builderApp.start();
				    BufferedReader commandInputApp = new BufferedReader(new InputStreamReader(stateRunApp.getInputStream()));
				    while ((lineApp = commandInputApp.readLine()) != null) {
				    	System.out.println(lineApp);
				    }
				    commandInputApp.close();
					
					// Corre el RIP
					ProcessBuilder builder = new ProcessBuilder();
					builder.directory(new File(commandRunDirectory));
					builder.command("cmd.exe", "/c", commandRun);
				    
					String line; 
				    Process stateRun = builder.start();
				    BufferedReader commandInput = new BufferedReader(new InputStreamReader(stateRun.getInputStream()));
				    while ((line = commandInput.readLine()) != null) {
				    	System.out.println(line);
				    }
				    commandInput.close();
				} catch (Exception e) {
				    e.printStackTrace();
				}
				
				System.out.println("Mutante "+(i+1)+" terminado");
			}
		}
	}
	
}
