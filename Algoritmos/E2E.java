package com.test.runner;

import java.io.File;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import app.SpringUtils;
import app.persistence.entity.App;
import app.persistence.entity.Dispositivo;
import app.persistence.entity.Ejecucion;
import app.persistence.entity.Nodo;
import app.persistence.entity.Suite;
import app.persistence.service.IEjecucionService;
import app.persistence.service.ISuiteService;
import app.persistence.service.SuiteService;
import app.persistence.service.EjecucionService;
import app.persistence.service.IAppService;
import app.persistence.service.AppService;

public class GenerateMutants2 extends Thread {
	
	private String appPackage;
	private Integer mutants; 
	private Long suiteID;
	
	@Autowired
	private ISuiteService suiteService;
	
	@Autowired
	private IAppService appService;
	
	@Autowired
	private IEjecucionService ejecucionService;
	
	
	public GenerateMutants2(String appPackage, Integer mutants, Long suiteID) {
		suiteService = (SuiteService)SpringUtils.context.getBean(SuiteService.class);
		appService = (AppService)SpringUtils.context.getBean(AppService.class);
		ejecucionService = (EjecucionService)SpringUtils.context.getBean(EjecucionService.class);
		
		this.appPackage = appPackage;
		this.mutants = mutants;
		this.suiteID = suiteID;
	}

	@Transactional
	public void run() {
		try {
			String mutantsFolderPath = "mutants";
			String tagToRunner = "@Mileage";
			
			// Suite a validar
			Suite originalSuite = suiteService.getOne(suiteID);
			Date fecha = new Date();
			Long fechaMilis = System.currentTimeMillis();
			
			// Ejecuta suite contra los mutantes
			File mutantsFolder = new File(mutantsFolderPath);
			File[] mutantsFiles = mutantsFolder.listFiles();
			if (mutantsFiles != null) {
				for (int i=0; i<mutants; i++) {
					
					// Crea app
					App appToSave = appService.getOne(originalSuite.getApp().getId());
					appToSave.setId(null);
					appToSave.setNombre(appToSave.getNombre()+"_Mutante"+(i+1)+"_"+fechaMilis);
					appToSave.setRutaApk(mutantsFolderPath+"/"+appPackage+"-mutant"+(i+1)+"/"+appPackage+"_3110-aligned-debugSigned.apk");
					appToSave.setMutation(true);
					App app = appService.insert(appToSave);
					
					// Crea suite
					Suite suiteToSave = suiteService.getOne(originalSuite.getId());
					suiteToSave.setId(null);
					suiteToSave.setNombre(suiteToSave.getNombre()+"_Mutante"+(i+1)+"_"+fechaMilis);
					suiteToSave.setApp(app);
					suiteToSave.setMutation(true);
					Suite suite = suiteService.insert(suiteToSave);
					
					// Crea ejecución
					Ejecucion ejecucion = new Ejecucion();
					Nodo nodo = new Nodo();
					nodo.setId(1L);
					ejecucion.setNodo(nodo);
					ejecucion.setSuite(suite);
					Dispositivo dispositivo = new Dispositivo();
					dispositivo.setId(14L);
					ejecucion.setDispositivo(dispositivo);
					ejecucion.setSegSleep(1);
					ejecucion.setSegTimeout(15);
					ejecucion.setHeadless(0);
					ejecucion.setEstado(0);
					ejecucion.setFecha(fecha);
					ejecucion.setEscenarios(tagToRunner);
					ejecucion.setExitosa(0);
					ejecucion.setMutation(true);
					ejecucionService.insert(ejecucion);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
