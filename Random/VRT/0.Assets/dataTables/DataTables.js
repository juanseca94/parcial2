/************************************************************
    EJEMPLO DE INICIALIZACIÓN
    
************************************************************/
/*
$(document).ready(function() {
	var table;
	var idTable = 'app_conductor';
	var serverSide = true; 
	var data = 'AjaxDataTable'; //ServerSide         
	//var serverSide = false; 
	//var data = 'Conductor/ExampleAjax.txt'; //ClientSide
	var type = 'fixed';
	var length = 5;  //Fixed: 5,10,25,50,100
	//var type = 'scroll';
	//var length = 5;  //Scroll: rows
	var info = true;
	var select = 'multi-select';
	var filterSearch = 'all';
	
	var columns = ["Cédula", "Nombre", "Apellidos", "Teléfono", "Dirección", "Ciudad", "Correo", "Licencia", "Transportadora"]; //0,1,2,3,4,5,6,7
	var actionNuevo = function(){
		window.location.href = "Conductores?action=nuevo";
	};
	var actionEditar = function(){
		var indexSelectedRow = table.row( { selected: true } ).index();
		var dataSelectedRow = table.row( { selected: true } ).data();
        window.location.href = "Conductores?action="+dataSelectedRow[0];
	};
	var actionEliminar = function(){
		var rowsSelected = table.rows( { selected: true } ).data();
		
		var params = "";
		var msg = "";
		var msg_resp = "";
		if(rowsSelected.length == 1) {
			msg += "el conductor: ";
			msg_resp = " ha sido eliminado"; 
		} else {
			msg += "los conductores: ";
			msg_resp = " han sido eliminados"; 
		}
		for (var i=0; i<rowsSelected.length; i++) {
			var dataSelectedRow = rowsSelected[i];
			params += dataSelectedRow[0];
			msg += dataSelectedRow[1] +" "+ dataSelectedRow[2];
			if (i < rowsSelected.length-2) {
				params += ";";
				msg += "‚ "; 
			} else if (i == rowsSelected.length-2) {
				params += ";";
				msg += " y "; 
			}
		}

        Alert("¡Confirmar acción!","¿Realmente desea eliminar "+msg+"?","confirm","Confirmar,Cancelar");
		var eliminar = document.getElementById('btnPrimaryAlert');
		eliminar.addEventListener('click',function(){
			var response = AJAX(params,null,'ELIMINAR_CONDUCTORES',false);
			if (response == "" || response == null) {
				Alert("¡Acción realizada!",FirstMayus(msg)+msg_resp+" con éxito.","alert","Aceptar");
				var aceptar = document.getElementById('btnPrimaryAlert');
				aceptar.addEventListener('click',function(){
					window.location.reload();
				})
			} else {
				Alert("¡Error!",response,"alert","Aceptar");
			}
		})
	};
	var buttons = [ // type: simple, select, export
		{type: "simple", name: "Nuevo", action: actionNuevo},
		{type: "select", name: "Editar", action: actionEditar},
		{type: "multi-select", name: "Eliminar", action: actionEliminar},
		{type: "export", nameField: "Conductores", orientation: 'landscape', pageSize: 'LETTER', messageTop: 'Mensaje superior', messageBottom: 'Mensaje superior'}
	];
	var parameters = [
		{name: "more_data", value: "my_value"},
		{name: "more_data2", value: "my_value2"}
	];
	//table = DataTable(idTable,columns,data,serverSide,type,info,length); // Simple
	//table = DataTable(idTable,columns,data,serverSide,type,info,length,filterSearch,select,buttons); // Complex
	table = DataTable(idTable,columns,data,serverSide,type,info,length,filterSearch,select,buttons,parameters); // Complex parameters
} );
*/




/************************************************************
    FUNCIONES
    
************************************************************/
function SinAcentos(cadena){
	var toFind = [
		'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â', 'ã', 'ä', 'å', 
		'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë',
		'Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï', 
		'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø',
		'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü', 
		'ÿ', 'Ç', 'ç', 'Ñ', 'ñ'
	];
	var toReplace = [
		'A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a',
		'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e',
		'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i',
	    'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o',
	    'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 
	    'y', 'C', 'c', 'N', 'n'
	];
	for (var i=0; i<toFind.length; i++) {
		cadena = cadena.replaceAll(toFind[i],toReplace[i]);
	}
	return  cadena;
}

String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};


/************************************************************
	VARIABLES DATATABLES
	
************************************************************/
var table;								//DataTable
var idTable;							//Id of table HTML
var columns;							//Columns of table
var columnsJson;						//Columns of Json
var data;								//Data source
var serverSide;							//ServerSide processing
var type;								//Type of table
var info;								//Info in table
var length;								//Size of table
var filterSearch;						//Filters search
var select;								//Type of select table
var buttons;							//Buttons of table
var parameters;							//Parameters

var sizeRow = 35; 						//Size row in pixels
var responsive = true;					//Responsive design
var scrollX = true;						//Scroll horizontal
$.fn.dataTable.ext.errMode = 'none';	//Disable alerts


/***********************************************************************************************
	TABLA CON TODAS LAS OPCIONES DE PERSONALIZACIÓN

	@idTable: 		Id de la tabla HTML.
	@columns: 		Nombres de las columnas de la tabla.
					  Ej: 
	@columnsJson	Nombre de etiquetas del JSON, Debe estar asociado uno a uno con columns
	@columnsMask	Funciones para renderizar el contenido de las columnas
	@data: 			Ruta desde raiz de donde vienen los datos.
		   			Ej: Servlet si es serverSide o del Txt si no lo es.
	@serverSide: 	Define si se los datos son dinámicos o estáticos.
					Ej: true, false
    @type: 			Tipo de presentación de la tabla.
    	   			Ej: fixed, scroll
    @info: 			Define si se muestra información sobre la cantidad de filas.
    	   			Ej: true, false
	@length: 		Tamaño de la tabla
	 				Ej: cantidad de filas si es fixed o catidad de pixeles si es scroll.
	@filterSearch: 	Define si la tabla tendra filtros de búsqueda.
					Ej: global, columns, all, none
	@select: 		Determina si la tabla tendra opción de seleccionar filas.
         			Ej: select, multi-select, none
	@buttons: 		Botones que tendra la tabla
					Ej: simple, select, multi-select, export(orientation: portrait,landscape - pageSize: A4,LETTER,LEGAL)
	@parameters: 	Variables ha enviar como parametro al servidor 
				 	Solo aplica para ServerSide.
***********************************************************************************************/
function DataTable(idTable,columns,columnsJson,columnsMask,data,serverSide,type,info,length,filterSearch,select,buttons,parameters) {
	if (serverSide) data += "?tb="+idTable;
	if (filterSearch == null) filterSearch = 'none';
	if (select == null) select = 'none';
	if (buttons == null) buttons = [];
	if (parameters == null) parameters = [];
	if (filterSearch == 'all' || filterSearch == 'columns') responsive = false;
	
	this.idTable = idTable;
	this.columns = columns;
	this.columnsJson = columnsJson;
	this.columnsMask = columnsMask;
	this.data = data;
	this.serverSide = serverSide;
	this.type = type;
	this.info = info;
	this.length = length;
	this.filterSearch = filterSearch;
	this.select = select;
	this.buttons = buttons;
	this.parameters = parameters;
	
	createTableHTML();
	createDataTable();
	posCssDataTable();
	
	return table;
}



/*************************************************************************
	CREA LA TABLA DATATABLE DE ACUERDO A PARAMETRIZACIÓN

*************************************************************************/
function createDataTable() {
    if (serverSide) {
        if (type == 'scroll') {
        	table = $('#'+idTable).DataTable( {
        		dom: typeDom(),
        		responsive: responsive,
        		processing: serverSide,
        		serverSide: serverSide,
        		sAjaxSource: data,
        	    fnServerParams: function (aoData) {
        	    	for (var i=0; i<parameters.length; i++) {
        	    		aoData.push(parameters[i]);
					}
        	    },
        		initComplete: function() {
                    var api = this.api();
                    api.columns().every(function() {
                        var column = this;
                        $('input', this.footer()).on('keyup change', function() {
                            if (column.search() !== this.value) {
                            	column.search(this.value).draw();
                            }
                        });
                    });
                },
        		aoColumns: createColumns(),
        		select: typeSelect(),
        		buttons: createButtons(),
        		language: language(),
                scrollY: length*sizeRow,
                scrollX: scrollX,
                scroller: {
                    loadingIndicator: true
                }
        	});
        }else if (type == 'fixed') {
        	table = $('#'+idTable).DataTable( {
        		dom: typeDom(),
        		lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'Todos']],
        		pageLength: length,
        		responsive: responsive,
        		processing: serverSide,
        		serverSide: serverSide,
        		sAjaxSource: data,
        	    fnServerParams: function (aoData) {
        	    	for (var i=0; i<parameters.length; i++) {
        	    		aoData.push(parameters[i]);
					}
        	    },
        		initComplete: function() {
                    var api = this.api();
                    api.columns().every(function() {
                        var column = this;
                        $('input', this.footer()).on('keyup change', function() {
                            if (column.search() !== this.value) {
                            	column.search(this.value).draw();
                            }
                        });
                    });
                },
        		aoColumns: createColumns(),
        		select: typeSelect(),
        		buttons: createButtons(),
        		language: language(),
				scrollX: scrollX
        	});
        }
    }else {
    	if (type == 'scroll') {
    		table = $('#'+idTable).DataTable( {
        		dom: typeDom(),
        		responsive: responsive,
        		processing: serverSide,
        		serverSide: serverSide,
        		ajax: data,
        		initComplete: function() {
                    var api = this.api();
                    api.columns().every(function() {
                        var column = this;
                        $('input', this.footer()).on('keyup change', function() {
                            if (column.search() !== this.value) {
                            	column.search(this.value).draw();
                            }
                        });
                    });
                },
        		aoColumns: createColumns(),
        		select: typeSelect(),
        		buttons: createButtons(),
        		language: language(),
        		scrollY: length*sizeRow,
                scrollX: scrollX,
                scroller: {
                    loadingIndicator: true
                }
        	});
        }else if (type == 'fixed') {
        	table = $('#'+idTable).DataTable( {
        		dom: typeDom(),
        		lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'Todos']],
        		pageLength: length,
        		responsive: responsive,
        		processing: serverSide,
        		serverSide: serverSide,
        		ajax: data,
        		initComplete: function() {
                    var api = this.api();
                    api.columns().every(function() {
                        var column = this;
                        $('input', this.footer()).on('keyup change', function() {
                            if (column.search() !== this.value) {
                            	column.search(this.value).draw();
                            }
                        });
                    });
                },
        		aoColumns: createColumns(),
        		select: typeSelect(),
        		buttons: createButtons(),
        		language: language(),
                scrollX: scrollX
        	});
        }
    }
}


/*************************************************************************
	AJUSTA EL CSS DE LOS ELEMENTOS DATATABLE DESPUES DE CREADOS

*************************************************************************/
function posCssDataTable() {
	setTimeout(function(){ 
		// Oculta el texto de filas seleccionadas si la tabla es tipo select none
		var selectItem = document.getElementsByClassName("select-item");
		for (var i=0; i<selectItem.length; i++) {
			if (select == 'none') {
				selectItem[i].style.display = "none";
			}
		}
		
		// Ajustar ancho de los filtros y columnas
//		var thTable = document.getElementsByTagName("th");
//		thTable[0].style.width = "10%";
	}, 500);
}


/*************************************************************************
    CREA TABLA HTML CON SUS RESPECTIVAS COLUMNAS
    
*************************************************************************/
function createTableHTML() {
    var tablaHTML = document.getElementById(idTable);
    tablaHTML.setAttribute("cellspacing", "0");
    tablaHTML.setAttribute("width", "100%");
    var thead = document.createElement("thead");
    var trh = document.createElement("tr");
    tablaHTML.appendChild(thead);
    thead.appendChild(trh);
    columns.forEach(function(item, index){
        var th = document.createElement("th");
        th.innerHTML = item.toUpperCase();
        trh.appendChild(th);
    });
    
    // Filters Columns
    if (filterSearch == 'columns' || filterSearch == 'all') {
    	// Footer    
        var tfoot = document.createElement("tfoot");
        var trf = document.createElement("tr");
        tablaHTML.appendChild(tfoot);
        tfoot.appendChild(trf);
        columns.forEach(function(item, i){
            var th = document.createElement("th");
            var title = item.toUpperCase();
            th.innerHTML = '<input type="text" id="fcol_'+(i++)+'" class="form-control dt-search" placeholder="'+title+'" title="Filtrar por '+title.toLowerCase()+'" />';
            trf.appendChild(th);
        });
    }
}


/*************************************************************************
	CONVERSIÓN DE LENGUAJE A ESPAÑOL

*************************************************************************/
function language() {
	var lang = {
		sProcessing: 		"Procesando...",
    	sLengthMenu: 		"Mostrar _MENU_ registros",
    	sZeroRecords:   	"No se encontraron resultados",
    	sEmptyTable:    	"Ningún dato disponible en esta tabla",
    	sInfo:          	"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    	sInfoEmpty:     	"Mostrando registros del 0 al 0 de un total de 0 registros",
    	sInfoFiltered:  	"(filtrado de un total de _MAX_ registros)",
    	sInfoPostFix:   	"",
    	sSearch:        	"Buscar:",
    	sUrl:           	"",
    	sInfoThousands: 	",",
    	sLoadingRecords:	"Cargando...",
    	oPaginate: {
    		sFirst:    		"Primero",
    		sLast:     		"Último",
    		sNext:     		"Siguiente",
    		sPrevious: 		"Anterior"
    	},
    	oAria: {
    		sSortAscending: ": Activar para ordenar la columna de manera ascendente",
    		sSortDescending:": Activar para ordenar la columna de manera descendente"
    	},
        select: {
            rows: {
                _: 			"Seleccionadas %d filas",
                1: 			"Seleccionada 1 fila"
            }
        },
        buttons: {
            copyTitle: 'Copiado en el porta papeles',
            copySuccess: {
                _: '%d filas copiadas',
                1: '1 fila copiada'
            }
        }
	};
	return lang;
}


/*************************************************************************
	DEFINE EL TIPO DE SELECT PARA LA TABLA

*************************************************************************/
function typeSelect() {
	var type = false;
    if (select != 'none') {
    	if (select == 'select') {
    		type = true;
        } else if (select == 'multi-select') {
        	type = {
    	    	style: 'multi'
    	    }
        }
    } 
	return type;
}


/*************************************************************************
	DEFINE EL TIPO DE DOM PARA LA TABLA DATATABLE

*************************************************************************/
function typeDom() {
	var dom = '';
    if (buttons.length > 0) {
    	dom += 'B';
    }
    if (filterSearch == 'global' || filterSearch == 'all') {
    	dom += 'f';
    }
    if (type == 'fixed') {
    	dom += 'rtlp';
    } else if(type == 'scroll') {
    	dom += 'rtp';
    }
    if (info) {
    	dom += 'i';
    }
	return dom;
}


/*************************************************************************
    CREA LAS COLUMNAS DE LA TABLA QUE SE ASOCIARAN A LA BASE DE DATOS
    
*************************************************************************/
function createColumns() {
	var columnsTemp = [];
	for (let i=0; i<columns.length; i++) {
		if (columnsMask[i] != null) {
			columnsTemp.push({sName: columns[i], mData: columnsJson[i], mRender: columnsMask[i]});
		} else {
			columnsTemp.push({sName: columns[i], mData: columnsJson[i]});
		}
	}
    return columnsTemp;
}


/*************************************************************************
    CREA LOS BOTONES QUE SE MOSTRARÁN SOBRE LA TABLA

*************************************************************************/
function createButtons() {
    var buttonsTemp = [];
    buttons.forEach(function(item, index){
        if(item['type'] === "simple") {
            buttonsTemp.push({text: item['name'], action: item['action'], titleAttr: item['name'], className: "btn-dt btn-dt-"+SinAcentos(item['name']).replaceAll(" ","-")});
        }
        if(item['type'] === "select") {
            buttonsTemp.push({extend: "selectedSingle", text: item['name'], titleAttr: item['name'], action: item['action'], className: "btn-dt btn-dt-"+SinAcentos(item['name']).replaceAll(" ","-")});
        }
        if(item['type'] === "multi-select") {
            buttonsTemp.push({extend: "selected", text: item['name'], titleAttr: item['name'], action: item['action'], className: "btn-dt btn-dt-"+SinAcentos(item['name']).replaceAll(" ","-")});
        }
        if(item['type'] === "export") {
            buttonsTemp.push({extend: 'collection', text: 'Exportar', titleAttr: 'Exportar', className: "btn-dt btn-dt-Exportar",
                buttons: [
                    { extend: 'copy', text: 'Copiar', title: item['nameField'], className: "li-dt btn-dt-copy"},
                    { extend: 'csv', text: 'CSV', title: item['nameField'], className: "li-dt btn-dt-csv"},
                    { extend: 'excel', text: 'Excel', title: item['nameField'], className: "li-dt btn-dt-excel"},
                    { extend: 'pdf', text: 'PDF', title: item['nameField'], className: "li-dt btn-dt-pdf", 
                    	messageTop: item['messageTop'], messageBottom: '\n'+item['messageBottom'], 
                    	orientation: item['orientation'], pageSize: item['pageSize']
        		    },
                    { extend: 'print', text: 'Imprimir', title: item['nameField'], className: "li-dt btn-dt-print", 
        		    	messageTop: item['messageTop'], messageBottom: '\n'+item['messageBottom']
    			    }
                ]
            });
        }
    });
    
    // Button refresh
    if (filterSearch != 'none') {
    	var actionRefresh = function(){
    		$("#"+idTable).DataTable().columns().every(function() {
                $('input', this.footer()).val('').change();
            });
    		$("#"+idTable).DataTable().search('').draw();
    	};
        buttonsTemp.push({text: '', action: actionRefresh, titleAttr: 'Refrescar', className: "btn-dt btn-dt-Refresh"});
    }
    
    return buttonsTemp;
}